package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HelloWorldController {
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView helloWorld() {
		ModelAndView mv = new ModelAndView("hello-world");
		mv.addObject("message", "Hello World!");
		return mv;
	}
	
	@RequestMapping(value="welcome", method = RequestMethod.GET)
	public ModelAndView welcome(@RequestParam(value = "name", required = false, defaultValue = "John Doe") String name) {
		ModelAndView mv = new ModelAndView("welcome");
		mv.addObject("name", name);
		return mv;
	}

}